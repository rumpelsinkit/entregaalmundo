package co.com.app.dispatchers;

import java.util.ArrayList;
import java.util.List;

import co.com.app.business.ProcesoLLamada;
import co.com.app.service.vo.LLamada;

/**
 * Clase que define plantilla para empleados
 * @author legyoroz
 *
 */
public abstract class Empleado {
	private Empleado asignarEmpleado;
	private String nombreEmpleado;
	private List<Empleado> empleados;
	private Boolean disponible;

	
	
	public abstract ProcesoLLamada atenderLLamada(Empleado emp ,LLamada llamada);
	public Empleado(){
		empleados = new ArrayList<Empleado>();
		disponible = true;
	}
	
	public Empleado getAsignarEmpleado() {
		return asignarEmpleado;
	}
	public void setAsignarEmpleado(Empleado asignarEmpleado) {
		this.asignarEmpleado = asignarEmpleado;
	}
	public String getNombreEmpleado() {
		return nombreEmpleado;
	}
	public void setNombreEmpleado(String nombreEmpleado) {
		this.nombreEmpleado = nombreEmpleado;
	}
	public List<Empleado> getEmpleados() {
		return empleados;
	}
	public void setEmpleados(List<Empleado> empleados) {
		this.empleados = empleados;
	}
	
	public void addEmpleado(Empleado emp){
		this.empleados.add(emp);
	}
	public Boolean getDisponible() {
		return disponible;
	}
	public void setDisponible(Boolean disponible) {
		this.disponible = disponible;
	}
	
    
}
