package co.com.app.business;

import java.util.Random;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.ThreadPoolExecutor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import co.com.app.dispatchers.Empleado;
import co.com.app.service.vo.LLamada;
@Service
public class CallManager {
	@Autowired
	public ThreadPoolTaskExecutor  threadPoolTaskExecutor;


	public CallManager(){

	}
	public LLamada atenderLLamada(Empleado empl,LLamada call){
		LLamada llamada = call; 
		llamada.setDuracionLLamada(generateDelay());
		if(empl != null){
			ProcesoLLamada pro = empl.atenderLLamada(empl,llamada);
			try{
				threadPoolTaskExecutor.setRejectedExecutionHandler(new RejectedExecutionHandler() {

					@Override
					public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
						System.out.println(((ProcesoLLamada)r).getIdllamada()+"error");

					}
				});
				threadPoolTaskExecutor.submit(empl.atenderLLamada(empl,llamada));
				
			}catch(Exception e){
				System.out.println("Excepcion atender llamada" +e.getMessage());
				
			}
			llamada.setNombreEmpleado(empl.getNombreEmpleado());
		}else{
			llamada.setMensaje("No hay empleados disponibles");

		}
		return llamada;
	}
	public int generateDelay(){
		Random generador = new Random();
		int delay = 5000+generador.nextInt(10*1000);

		return delay;
	}
}
