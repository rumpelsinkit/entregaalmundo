package co.com.app.business;

import co.com.app.dispatchers.Empleado;
import co.com.app.service.vo.LLamada;

public class ProcesoLLamada implements Runnable {
    
	private int delay;
	private Empleado empl;
	private int idllamada;
	
	
	public ProcesoLLamada(Empleado empl,LLamada llamada) {
		delay = llamada.getDuracionLLamada();
		this.empl = empl;
		idllamada = Integer.parseInt(llamada.getCallId());
	}
	
	@Override
	public void run() {

       try{
    	   System.out.println("ejecutando idllamada:"+ String.valueOf(idllamada)+" empleado:"+empl.getNombreEmpleado()+" Estado:"+empl.getDisponible());   
    	   Thread.sleep(delay);
    	   empl.setDisponible(true);
    	   System.out.println("fin idllamada:"+ String.valueOf(idllamada)+" empleado:"+empl.getNombreEmpleado()+" Estado:"+empl.getDisponible());
    	   
       }catch(Exception e){   
    	   e.printStackTrace();
    	   System.out.println("idllamada "+idllamada+" empleado "+empl.getNombreEmpleado()+e.getMessage());
       }		
	}

	public int getIdllamada() {
		return idllamada;
	}

	public void setIdllamada(int idllamada) {
		this.idllamada = idllamada;
	}
   
}
