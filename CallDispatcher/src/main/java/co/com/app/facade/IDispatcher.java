package co.com.app.facade;

import co.com.app.service.vo.LLamada;

public interface IDispatcher {
   
   
	public LLamada dispatch(LLamada llamada);
}
