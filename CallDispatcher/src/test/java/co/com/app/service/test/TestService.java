package co.com.app.service.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.context.request.async.DeferredResult;

import co.com.app.business.CallManager;
import co.com.app.dispatchers.Director;
import co.com.app.dispatchers.Empleado;
import co.com.app.dispatchers.Operador;
import co.com.app.dispatchers.Supervisor;
import co.com.app.facade.Dispatcher;
import co.com.app.facade.IDispatcher;
import co.com.app.service.CallService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:spring-test-configuration.xml"})
@WebAppConfiguration
public class TestService {
	
	 private IDispatcher dispathcher;
     private CallService service;
     private CallManager manager; 
     @Autowired
     @Qualifier("threadPoolTaskExecutor")
     private ThreadPoolTaskExecutor threadpool;
    
     @Before
     public void init(){
    	 service = new CallService();
    	 dispathcher = new Dispatcher();
    	 manager = new CallManager();
    	 //Empleado rsp = cargaCadenaResponsabilidad(6,3,1);	 
    	 
    	// ReflectionTestUtils.setField(dispathcher, "attenders", rsp);
    	 ReflectionTestUtils.setField(manager, "threadPoolTaskExecutor", threadpool);
    	 ReflectionTestUtils.setField(dispathcher, "manager", manager);
    	 ReflectionTestUtils.setField(service, "dispatcher", dispathcher);
    	 
    	 
     }
	   
	@Test
	public void testService() {
		List<String> llmd= calls(10);
		for(String t:llmd){
			DeferredResult<String> r = new DeferredResult<>(); 
			r =	service.dispachCall(t);
		}
		while(true){
			int count = threadpool.getActiveCount();
			System.out.println("Active Threads : " + count);
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if (count == 0) {
				threadpool.shutdown();
				break;
			}
		}
	}
	/**
	 * Crea la cadena de responsabilidad o gerarquia de recepcion de llamadas
	 * @return
	 */
	public Empleado cargaCadenaResponsabilidad(int operadores,int supervisores,int directores){
		Empleado operador= new Operador();
		List<Empleado> opres = new ArrayList<>();
		List<Empleado> supres = new ArrayList<>();
		List<Empleado> dirtores = new ArrayList<>();
		for(int i=1;i<=operadores;i++){
			Empleado op = new Operador();
			op.setNombreEmpleado(Operador.tipo+i);
			opres.add(op);
		}
		for(int i=1;i<=supervisores;i++){
			Empleado sup = new Supervisor();
			sup.setNombreEmpleado(Supervisor.tipo+i);
			supres.add(sup);
		}
		for(int j=1;j<=directores;j++){
			Empleado dir = new Director();
			dir.setNombreEmpleado(Director.tipo+j);
			dirtores.add(dir);
		}
		operador.setEmpleados(opres);
		Empleado supervis = new Supervisor();
		supervis.setEmpleados(supres);
		Empleado direct = new Director();
		direct.setEmpleados(dirtores);
		supervis.setAsignarEmpleado(direct);
		operador.setAsignarEmpleado(supervis);
		return operador;
	}
	/**
	 * Genera diferentes request para enviar al servicio
	 * @param reqs
	 * @return
	 */
	public List<String> calls(int reqs){
		List<String> llamados = new ArrayList<>();
		for(int i=1;i<=reqs;i++){
			llamados.add("{\"callId\":\""+i +"\",\"nombreEmpleado\":\"\",\"duracionLLamada\":\"\",\"mensaje\":\"\"}");
		}
		return llamados;
	}

}
