package com.almundo.client;

import java.util.concurrent.Future;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.InvocationCallback;
import javax.ws.rs.client.WebTarget;

public class ClientDispatcher {

	public static void main(String[] args) {
		Client client = ClientBuilder.newBuilder().build();
		//"http://localhost:8080/CallDispatcher/serviceDispatch/DispatchCall/{\"callId\":\""+1 +"\",\"nombreEmpleado\":\"\",\"duracionLLamada\":\"\",\"mensaje\":\"\"}");
		WebTarget target= client.target("http://localhost:8080/RestServ/service/rest/srvtest/msgAsync/dd");
		//http://localhost:8080/RestServ/service/rest/srvtest/msgAsync/dd
		Future<String> responseFuture = target
		        .request().async().get(new InvocationCallback<String>() {
		            @Override
		            public void completed(String response) {
		                System.out.println("Response status code "
		                        + response + " received.");
		            }
		 
		            @Override
		            public void failed(Throwable throwable) {
		                System.out.println("Invocation failed.");
		                throwable.printStackTrace();
		            }
		        });

	}
	

}
